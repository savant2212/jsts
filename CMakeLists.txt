cmake_minimum_required(VERSION 2.8)

project (jsts)
set(CMAKE_BUILD_TYPE Release)
add_definitions(-DUSE_INTERFACE)

add_library(jsts SHARED jsts.c)
target_link_libraries(jsts tinyscheme json-c)

set_target_properties(jsts PROPERTIES PREFIX "")

install(TARGETS jsts DESTINATION lib)
