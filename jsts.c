#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <scheme-private.h>
#include <scheme.h>
#include <json-c/json_object.h>
#include <json-c/json_tokener.h>
#include <json-c/json_object_iterator.h>

pointer jsts_error;
pointer jsts_object;

pointer __TS_ERROR(scheme* sc, char* fmt,...){
        va_list args;
        va_start(args, fmt);
        int size;
        char* buf;
        pointer retval;

        size = vsnprintf(NULL, 0, fmt, args) + 1;
        buf = malloc(size);
        vsnprintf(buf, size, fmt, args);
        va_end(args);
        retval = cons(sc, jsts_error, mk_string(sc, buf));
        free(buf);
        return retval;
}

#define JSTS_ERROR(s) __TS_ERROR(sc, (s))
#define JSTS_ERROR_FMT(fmt,...) __TS_ERROR(sc, (fmt), __VA_ARGS__)

static pointer parse_json_object(scheme*,json_object*);
static pointer parse_json_array(scheme*,json_object*);

static pointer parse_json_value(scheme* sc, json_object* o){
	switch( json_object_get_type(o) ){
		case json_type_boolean:
			if( json_object_get_boolean(o) ) {
				return sc->T;
			} else {
				return sc->F;
			}
			break;
		case json_type_double:
			return mk_real(sc,  json_object_get_double(o));
			break;
		case json_type_int:
			return mk_integer( sc, json_object_get_int(o));
			break;
		case json_type_string:
			return mk_counted_string( sc, json_object_get_string(o), json_object_get_string_len(o));
			break;
		case json_type_object:
			return cons(sc, parse_json_object(sc, o), sc->NIL);
			break;
		case json_type_array:
			return parse_json_array(sc, o);
			break;
	}
	return sc->NIL;
}

static pointer parse_json_array( scheme* sc, json_object* js){
	if( js == NULL || json_object_get_type(js) != json_type_array ) return sc->NIL;

	int len = json_object_array_length(js);

	if( len <= 0 ) { return sc->NIL;}

	pointer vec = sc->vptr->mk_vector(sc, len);
	int i = 0;

	for( i=0; i < len; i++ ){
		sc->vptr->set_vector_elem(
				vec,
				i,
				parse_json_value(
					sc, json_object_array_get_idx(js, i)
				)
		);
	}

	return vec;
}

static pointer parse_json_object( scheme* sc, json_object* js){
	pointer retval = sc->NIL;

	if( js == NULL ) return retval;

	struct json_object_iterator it;
	struct json_object_iterator itEnd;

	it = json_object_iter_begin(js);
	itEnd = json_object_iter_end(js);
	pointer val;
   	val	= sc->NIL;


	while (!json_object_iter_equal(&it, &itEnd)) {

		struct json_object* o = json_object_iter_peek_value(&it);

		val = cons( sc,
						cons( sc,
							mk_string( sc, json_object_iter_peek_name(&it) ),
							parse_json_value(sc, o)
						),
						val
				);

		json_object_iter_next(&it);
	}

	retval = val;

	return retval;
}

pointer jsts_parse(scheme* sc, pointer args){
	pointer retval;
	if( args == sc->NIL ){
		return JSTS_ERROR("Must be at least 1 argument");
	}
	pointer p = pair_car(args);

	if( ! is_string(p)) return JSTS_ERROR("First argument must be json string");

	json_object *jobj = NULL;
	const char *mystring = NULL;
	int stringlen = 0;
	enum json_tokener_error jerr;
	struct json_tokener* tok = json_tokener_new();

	do {
        jobj = json_tokener_parse_ex(tok, string_value(p), strlen(string_value(p)));
	} while ((jerr = json_tokener_get_error(tok)) == json_tokener_continue);

	json_tokener_free(tok);

	if (jerr != json_tokener_success) {
		retval = JSTS_ERROR_FMT( "JSON parsing error. %s", json_tokener_error_desc(jerr));
	} else {
		retval = parse_json_object( sc, jobj );
	}

	return retval;
}

void init_jsts(scheme* sc){
	jsts_error 	= mk_symbol(sc, "js_error");
	jsts_object = mk_symbol(sc, "js_object");
	scheme_define(
			sc,
			sc->global_env,
			mk_symbol(sc, "json_parse"),
			mk_foreign_func(sc, jsts_parse)
	);

	return;
}
