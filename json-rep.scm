(define test-j 
  (list
	( cons "string"  "string value")
	( cons "object"  (list 1 2 3))
	( cons "numeric"  12345)
	( cons "array"  (vector 1 2 3 4 5))
	( cons "true"  #t )
	( cons "false"  #f )
	( cons "null"  () )
  )
)
